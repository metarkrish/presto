package util;

import java.util.ArrayList;
import java.util.Arrays;

public class TestUtils {
    @SafeVarargs
    public static <E> ArrayList<E> listOf(E... elements){
        return new ArrayList<>(Arrays.asList(elements));
    }
}
