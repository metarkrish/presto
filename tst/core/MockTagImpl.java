package core;

import java.util.Objects;
import java.util.Random;

public class MockTagImpl implements Tag {
    private String name;

    public MockTagImpl() {
        name = String.valueOf(new Random().nextInt());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MockTagImpl mockTag = (MockTagImpl) o;
        return name.equals(mockTag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
