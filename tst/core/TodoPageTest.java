package core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static util.TestUtils.listOf;

public class TodoPageTest {
    private TodoPage todoPage;
    private ArrayList<TaskItem> taskItems;
    private ArrayList<Tag> tags; // TODO: 06/09/20 Utilize this

    @BeforeEach
    void setUp() {
        taskItems = listOf(new TaskItem(), new TaskItem(), new TaskItem());
        tags = listOf(new MockTagImpl(), new MockTagImpl());
        todoPage = new TodoPage(taskItems, tags);
    }

    @Test
    void givenATaskItem_addTaskItem_successfullyAddsAndReturnsTheItem() {
        TaskItem taskItem = new TaskItem();

        TaskItem returnedTaskItem = todoPage.addTaskItem(taskItem);

        assertEquals(taskItem, returnedTaskItem);
        assertTrue(taskItems.contains(taskItem));
    }

    @Test
    void givenTodoPageWithItems_hasTaskItem_successfullyChecksForItemPresent() {
        TaskItem item = taskItems.get(1);

        assertTrue(todoPage.hasTaskItem(item));
    }

    @Test
    void givenTodoPageWithThreeItems_removeTaskItem_resultsInTwoItems() {
        TaskItem item2 = taskItems.get(1);

        boolean wasDeleted = todoPage.removeTaskItem(item2);

        assertTrue(wasDeleted);
        assertEquals(2, taskItems.size());
        assertFalse(taskItems.contains(item2));
    }

    @Test
    void givenDefaultTodoPageConstruction_itemCount_returns0() {
        TodoPage freshTodoPage = new TodoPage();

        assertEquals(0, freshTodoPage.itemCount());
    }

    @Test
    void givenTodoPageWithItems_itemCount_getsCorrectTotalItemCount() {
        assertEquals(3, todoPage.itemCount());
    }

    @Test
    void givenTodoPageWithTasks_listTasks_getsAllTasks() {
        List<TaskItem> tasks = todoPage.listTasks();

        assertEquals(this.taskItems, tasks);
    }

    @Test
    void givenTodoPageWithTasks_clearPage_emptiesThePage() {
        todoPage.clearPage();
        assertEquals(0, taskItems.size());
    }

    @Test
    void givenTodoPageWithTwoTags_addTag_AddsTheThirdTag() {
        todoPage.addTag(new MockTagImpl());
        assertEquals(3, todoPage.listTags().size());
    }

    @Test
    void givenTodoPageWithTwoTags_addTagForAnExistingTag_doesNotChangeTagSet() {
        todoPage.addTag(tags.get(0));
        assertEquals(2, todoPage.listTags().size());
    }
    @Test
    void givenTodoPageWithTwoTags_listTags_retrievesTheTags() {
        Set<Tag> tagSet = todoPage.listTags();
        assertEquals(Set.copyOf(tags), tagSet);
    }
}
