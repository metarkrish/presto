package core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TodoPage {
    private final ArrayList<TaskItem> taskItems;
    private final Set<Tag> tags;

    public TodoPage() {
        taskItems = new ArrayList<>();
        tags = new HashSet<>();
    }

    public TodoPage(ArrayList<TaskItem> taskItems, ArrayList<Tag> tags) {
        this.taskItems = taskItems;
        this.tags = new HashSet<>(tags);
    }

    public TaskItem addTaskItem(TaskItem taskItem) {
        taskItems.add(taskItem);
        return taskItem;
    }

    public boolean hasTaskItem(TaskItem taskItem) {
        return taskItems.contains(taskItem);
    }

    public boolean removeTaskItem(TaskItem taskItem) {
        return taskItems.remove(taskItem);
    }

    public int itemCount() {
        return taskItems.size();
    }

    public List<TaskItem> listTasks() {
        return taskItems;
    }

    public void clearPage() {
        taskItems.clear();
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }


    public Set<Tag> listTags() {
        return tags;
    }
}